use std::str::{CharIndices, Chars};
use std::collections::{BTreeMap, BTreeSet};

type CharMap<T> = BTreeMap<char, T>;
type ValueSet<T> = BTreeSet<T>;

trait GetOrCompute<K, V> {
    fn get_or_compute<F>(&mut self, key: &K, func: F) -> &mut V
    where F: Fn(&K) -> V;
}

impl<T> GetOrCompute<char, T> for CharMap<T> {
    fn get_or_compute<F>(&mut self, key: &char, func: F) -> &mut T
    where F: Fn(&char) -> T {
        if !self.contains_key(key) {
            let value = func(key);
            self.insert(*key, value);
        }
        self.get_mut(key).unwrap()
    }
}

#[derive(Debug)]
struct Node<T> {
    values: ValueSet<T>,
    children: CharMap<Box<Node<T>>>
}

impl<T> Default for Node<T> {
    fn default() -> Self {
        Self { values: Default::default(), children: Default::default() }
    }
}

pub struct PathIterator<'a, T> {
    chars: CharIndices<'a>,
    node: &'a Node<T>
}

impl<'a, T> Iterator for PathIterator<'a, T> {
    type Item = (usize, char, &'a ValueSet<T>);

    fn next(&mut self) -> Option<Self::Item> {
        let (offset, char) = self.chars.next()?;
        self.node = self.node.children.get(&char)?;
        return Some((offset + char.len_utf8(), char, &self.node.values));
    }
}

#[derive(Debug)]
pub struct TextIndex<T> {
    root: Node<T>
}

impl<T: Ord> TextIndex<T> {
    pub fn new() -> Self {
        Self { root: Default::default() }
    }

    pub fn insert<S: AsRef<str>>(&mut self, key: S, value: T) {
        let mut node: &mut Node<T> = &mut self.root;

        for c in key.as_ref().chars() {
            node = node.children.get_or_compute(&c, |_| Box::new(Node::<T>::default()));
        }

        node.values.insert(value);
    }

    pub fn get_values<S: AsRef<str>>(&self, key: S) -> Option<&ValueSet<T>> {
        let mut node: &Node<T> = &self.root;

        for c in key.as_ref().chars() {
            match node.children.get(&c) {
                Some(n) => node = n,
                None => return None,
            }
        }

        Some(&node.values)
    }

    pub fn iter_path<'a>(&'a self, path: &'a str) -> PathIterator<'a, T> {
        PathIterator { chars: path.char_indices(), node: &self.root }
    }
}

#[cfg(test)]
mod tests {
    use crate::{TextIndex, ValueSet};

    #[test]
    fn insert_and_get() {
        let mut index = TextIndex::<i32>::new();
        index.insert("Hello world!", 42);
        index.insert("Hello world!", 64);
        index.insert("Hello", 99);
        let values = index.get_values("Hello world!");
        
        let mut expected = ValueSet::<i32>::new();
        expected.insert(42);
        expected.insert(64);

        assert_eq!(Some(&expected), values);
    }

    #[test]
    fn insert_and_iterate() {
        let mut index = TextIndex::<i32>::new();
        index.insert("こんにちは、世界！", 1);
        index.insert("こんにちは", 2);
        let iter = index.iter_path("こんにちは、世界！ ^w^");
        for entry in iter {
            println!("{entry:?}");
        }
    }
}
